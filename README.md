# Cigorette

## Installing
```bash
$ sudo add-apt-repository ppa:kip/nicotine+
$ sudo add-apt-repository ppa:lilacashes/cigorette
$ sudo apt install cigorette
```

## Connecting to the Soulseek network via tor

1. Run the script installed by the package, `/usr/bin/nicotine-tor.sh`.
2. If you haven't run nicotine before, a dialog will guide you through setting 
   it up.
3. Under the menu item Edit/Settings, select "Server"
4. Change "server.slsknet.org:2242" to "localhost:22400"
5. To verify you are connected via tor:
   1. Select "Shares/Browse My Shares" from the menu
   2. In the tab "User browse", your shares should appear
   3. Right click your user name and select "Show IP" in the context menu
   4. The IP shown in the message window at the bottom needs to be different 
      from what a service such as https://www.whatismyip.com/ shows you.