#!/usr/bin/fish
for distribution in bionic disco eoan
    set VERSION 0.1.XXX
    rm -rf cigorette-{$VERSION}
    env PACKAGE_NAME=cigorette DISTRIBUTION=$distribution MAINTAINER_EMAIL='lene.preuss@gmail.com' \
        LICENSE='gpl3' CIGORETTE_URL='https://gitlab.com/lilacashes/nicotine-over-tor' \
        CIGORETTE_DESC="Cigorette contains nicotine and tor." \
        CIGORETTE_LONGDESC="Connects to the Soulseek filesharing network anonymously." \
        PGP_KEY_ID='XXX' \
        ./create_deb.sh --version $VERSION --source
    echo uploading cigorette_$VERSION-1~"$distribution"_source.changes
    dput -f ppa:lilacashes/cigorette cigorette_$VERSION-1~"$distribution"_source.changes; or break
end
