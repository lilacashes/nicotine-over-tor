apt update && \
  apt install -y software-properties-common python-apt python3-requests && \
  add-apt-repository -y ppa:kip/nicotine+ && \
  add-apt-repository -y ppa:lilacashes/cigorette && \
  apt update && \
  export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true && \
  apt install -y cigorette && \
  nicotine -v && \
  grep '"server": (' /usr/lib/python2.7/dist-packages/pynicotine/config.py
