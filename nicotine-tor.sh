#!/bin/bash

which nicotine >/dev/null || sudo apt install nicotine
which tor >/dev/null || sudo apt install tor
which socat >/dev/null || sudo apt install socat
socat TCP4-LISTEN:22400,fork SOCKS4A:localhost:server.slsknet.org:2242,socksport=9050 >/dev/null &
nicotine "$@"
