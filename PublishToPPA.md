# Publishing your software on a PPA on launchpad.net
If you want to publish some Software you are writing as a .deb package for Ubuntu and compatible
Linux distributions, you can of course try to get the Debian or Ubuntu core teams to include your
software in their distribution. <link to how that is done and what the requirements are> 

Or you can go a slightly easier way and publish your software on a Personal Package Archive, in 
which case the only person you need to convince that your package is worth installing is the end 
user. Installing software from a PPA is slightly more complicated than from the official 
repositories, but it is still straightforward. Instead of just typing
```bash
$ sudo apt install <your_package>
``` 
as is done with packages included in the distribution, the user must set up your PPA as a valid 
repository first, and type
```bash
$ sudo add-apt-repository ppa:<your/ppa_name>
$ sudo apt update
``` 
before typing 
```bash
$ sudo apt install <your_package> 
```
After the PPA is set up with `add-apt-repository`, the user will not have to do it again though, and 
your package will receive upgrades each time the user does an `apt upgrade` - so it behaves like any
other package installed with the distribution.

The process of setting up a PPA and publishing your software to it is somewhat painful, though - 
especially since there is no comprehensive documentation on how to do it. This document attempts to 
change that.

## Setting up a PPA

### Create a Launchpad user

Ubuntu PPAs are hosted on launchpad.net. If you already have a Launchpad user, you can skip this 
step.

Go to https://launchpad.net. There is a link to Login/register at the top right. It will redirect 
you to the Login/Register page for Ubuntu One - slightly confusing, but you are still on the right
track. If you are new, select "I don't have an Ubuntu One account" and follow the registration 
process.
  
### Create a PPA for your project

After logging in to Launchpad, you see a link to your profile page in the top right corner. On your
profile page there is a section called "Personal package archives", with a link to create a new PPA.
You will be asked to enter a URL and a display name for your PPA. 

Assuming you called your PPA `my_project` and your Launchpad username is `my_user`, your PPA's name
will be `ppa:my_user/my_project`. People will be able to install software from your PPA after they 
registered it with
```bash
$ sudo add-apt-repository ppa:my_user/my_project
``` 
* If your PPA depends on software that is published on another PPA, you can add that dependency 
  under the menu point "Edit PPA dependencies" on the top right of the page for your PPA. If you add
  a PPA here, it will automatically be installed and upgraded with your package.
 
## Setting up a PGP key for your Launchpad PPA

Launchpad will not accept uploads to your PPA unless you have signed your upload with a PGP key. 
Here is how to set up a key for Launchpad:

### Creating a new key
If you want to reuse an existing PGP key, you can skip this step. If you want to publish to your PPA
from any kind of CI pipeline, you will have to upload your secret key to the CI server though. In 
that case you should definitely create a separate key just for publishing to the PPA.

Create a new PGP key:
```bash
$ gpg --full-generate-key
```
Generate an RSA/RSA pair and use a key length of at least 3072.
**If you want to use your key in a CI pipeline later, do not enter a passphrase.**

Has your key been generated?
```bash
$ gpg --list-secret-keys --keyid-format LONG
[...]
sec   rsa4096/YOUR_KEY_ID..... 2020-01-20 [SC]
      YOUR_KEY_FINGERPRINT....................
uid                 [ultimate] Your Name <your_name@your_email.com>
ssb   rsa4096/SECRET_SUB_KEY.. 2020-01-20 [E]
```

### Upload the key to the Ubuntu keyserver
Upload your key to the Ubuntu keyserver:
```bash
$ gpg --keyserver keyserver.ubuntu.com --send-keys YOUR_KEY_FINGERPRINT_OR_ID..............
gpg: sending key YOUR_KEY_ID..... to hkp://keyserver.ubuntu.com
```

Has the key arrived at the Keyserver?
```bash
$ gpg --keyserver hkp://keyserver.ubuntu.com --search-key 'your_name@your_email.com'
gpg: data source: http://162.213.33.9:11371
(1)     Your Name <your_name@your_email.com>
          4096 bit RSA key YOUR_KEY_ID....., created: 2020-01-20
Keys 1-1 of 1 for "your_name@your_email.com".  Enter number(s), N)ext, or Q)uit > q
```
According to the [Launchpad documentation](https://launchpad.net/+help-registry/openpgp-keys.html),
it can take up to 30 minutes until the key is available.
 
### Import the key into Launchpad
On your Launchpad profile page (https://launchpad.net/~your_launchpad_username) there is a section 
"*OpenPGP keys*" with an edit icon. The edit icon takes you to the page 
https://launchpad.net/~your_launchpad_username/+editpgpkeys. 

There, under the section "*Import an OpenPGP key*", you can enter the fingerprint of your PGP key.

To get your PGP fingerprint:
```bash
$ gpg --fingerprint your_name@your_email.com
pub   rsa4096 2020-01-20 [SC]
      XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX
uid           [ultimate] Your Name <your_name@your_email.com>
sub   rsa4096 2020-01-20 [E]
``` 
The group of ten 4-digit hexadecimal numbers denoted by "XXXX"s above is your fingerprint.

After uploading the fingerprint, you either get an error that your key is not yet available (in that
case, if you have generated and uploaded your key properly, as described above, all you have to do 
is wait a few minutes and retry. Remember, it may take up to 30 minutes for your uploaded key to be
available.) or you will get an email to your_name@your_email.com, encrypted with your key, that 
tells you how to proceed.

To decrypt it, either use your mail client, if it supports PGP - Thunderbird with the Enigmail 
plugin would be a typical choice here. If your mail client does not support it, you can copy/paste
the text from `-----BEGIN PGP MESSAGE-----` to `-----END PGP MESSAGE-----` and save it to a file, 
e.g. `encrypted_message.txt` and then use
```bash
$ gpg -d encrypted_message.txt
```
to read the message. The message should give you a URL to visit. Under that URL you will be asked to
confirm your PGP key once more.

After confirming, your key should be listed in the "*OpenPGP keys*" in your Launchpad profile. 

## Generating, signing and uploading a .deb file

### Create build directory

You need to create an image of the files you want your .deb to install to `/` of the target system.
This is done in the build directory, which you can call e.g. `<your_package>-0.1`.
```bash
$ cd <your_project_root>
$ mkdir <your_package>-0.1
$ mkdir -p <your_package>-0.1/usr/bin
$ cp <your_executable> <your_package>-0.1/usr/bin
$ mkdir -p <your_package>-0.1/usr/share/doc
$ cp <your_documentation> <your_package>-0.1/usr/share/doc
```

### Running `dh_make` to generate package metadata
Then you can use `dh_make` to create a template of the files that are needed to create metadata for 
your .deb:  
```bash
$ cd <your_package>-0.1
$ dh_make --createorig
```
You have a few options to dh_make here that can save you a bit of typing and editing later on:
* `--email your_name@your_email.com` inserts your email as maintainer email in all needed places
* `--copyright <COPYRIGHT>` sets the license, where <COPYRIGHT> can be one of 'apache', 'artistic', 
  'bsd', 'gpl', 'gpl2', 'gpl3', 'isc', 'lgpl', 'lgpl2', 'lgpl3', 'mit', 'custom'.
* `--single|--indep|--library` set the package class to single binary, architecture independent
  binary or library
* `--yes` skips all confirmation questions which can be extra handy if you run `dh_make` from a 
  script or a CI pipeline  
So a typical call to `dh_make` would look like:
```bash
$ dh_make --indep --email "your_name@your_email.com" --copyright gpl3 --createorig --yes
```

### Cleaning up de `debian` folder

`dh_make` creates the `debian` subfolder, but you need to edit some of the generated files a bit to 
suit your package.

* `changelog`: If this is your first published version you don't need to edit much, if you have 
  given `dh_make` all necessary information via the switches mentioned above. For every version 
  published after, add a line that summarizes the changes between the versions.
  One thing you need to set though is the target distribution:
  ```bash
  $ sed -i "s#unstable#<YOUR TARGET DISTRIBUTION>#" debian/changelog
  ```   
  If you want to upload a source deb for multiple distributions, you'll have to generate and upload 
  it separately for each distro.
* `control`: This needs a little more work. There are three lines which ...
  ```bash
  $ sed -i "s#Section: unknown#Section: web#" debian/control
  $ sed -i 's#Depends: ${misc:Depends}#Depends: <PACKAGES YOUR PROJECT'S DEPENDS ON>#' debian/control
  $ sed -i "s#Homepage: <insert the upstream URL, if relevant>#<YOUR PROJECT'S HOMEPAGE>#" debian/control
  $ sed -i "s#Description: <insert up to 60 chars description>#<YOUR PROJECT'S SHORT DESCRIPTION>#" debian/control
  $ sed -i "s# <insert long description, indented with spaces># <YOUR PROJECT'S LONG DESCRIPTION>#" debian/control
  ```
  Yor project's long description can be multiple lines long. Take care though, that every line of 
  the long description, including the first, starts with a space.
* `source`: If you want to save yourself a lot of pain when packaging, change the source .deb format
  from `quilt` to `native`:
  ```bash
  $ sed -i 's#quilt#native#' debian/source/format
  ```
* You can very likely remove all the auto-generated examples:
  ```bash
  $ rm debian/*.ex debian/*.EX
  ```
* Unless you want to provide your own version of `README.source`, you should also delete it:
  ```bash
  $ rm debian/README.source
  ```  

### Test Step: Building a binary .deb package
To check if your edits have been sufficient, enter 
```bash
$ dpkg-deb --build <your_package>-0.1
```
You can then test if the debfile that has been created works correctly by typing
```bash
$ sudo apt install ./<your_package>_0.1.deb
```
Bonus: if you have `docker` installed (docker installation is beyond the scope of this document, 
see https://docs.docker.com/install/ for instructions), you can run
```bash
$ docker run --mount type=bind,source="$(pwd)",target=/app -it ubuntu:18.04 bash
```
which drops you into a freshly installed Ubuntu 18.04 from where you can try the above command 
(after an `apt update`):
```bash
$ apt update
$ apt install /app/<your_package>_0.1.deb
``` 
That way you are secure against messing up your own system, and you can also see whether your .deb 
file makes any assumptions about software that is installed on your system, but might not be there
on other users' systems.

### Building the source .deb package
```bash
$ cd <your_package>-0.1
$ debuild -S -sd
```
If your key has a passphrase you will be asked for it.

This will generate a number of files in your project root directory.

If you want to separate the signing of the sources from the build process, you can also run 
`debsign` manually after building:
```bash
$ cd <your_package>-0.1
$ debuild -S -us -uc
$ cd ..
$ debsign -k <your_key_id> <your_package>_source.changes
$ debsign -k <your_key_id> <your_package>.dsc
```


### Upload to Launchpad
If you have built and signed your package correctly, you can upload it using `dput` from the root of
your project: 
```bash
$ dput -f ppa:<your/ppa_name> <your_package>_source.changes
```

### Did it work?
There is a number of things that can go wrong when using `dput`. Sometimes it gives you an error 
message immediately. Other times it uploads without errors, but still fails. You will receive a mail
with the upload status, Accepted or Rejected. 

See https://help.launchpad.net/Packaging/UploadErrors for help if your upload is rejected.

## Bonus track: Integrating into Gitlab CI 
### Import secret key ### Build source .deb 
### Upload source .deb
