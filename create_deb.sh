#!/bin/bash

EXECUTABLE="nicotine-tor.sh"

while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        --version)
        PACKAGE_VERSION="$2"
        shift # past argument
        shift # past value
        ;;
        --source)
        SOURCE="true"
        shift
        ;;
        *)    # unknown option
        echo "unknown option: $1"
        exit 1
        ;;
    esac
done

if [[ "x${PACKAGE_VERSION}" == "x" ]]; then
    echo "--version is required"
    exit 1
fi

BUILD_DIR="cigorette-${PACKAGE_VERSION}"
USR_BIN="${BUILD_DIR}/usr/bin"

mkdir -p $USR_BIN
cp ${EXECUTABLE} $USR_BIN
cp ${EXECUTABLE} $BUILD_DIR

function postinst() {
    DEBIAN=$1
    echo "sed -i \"s/'server.slsknet.org', 2242/'localhost', 22400/\" /usr/lib/python3/dist-packages/pynicotine/config.py" > $DEBIAN/postinst
    echo "sed -i \"s/'localhost', 22400/'server.slsknet.org', 2242/\" /usr/lib/python3/dist-packages/pynicotine/config.py" > $DEBIAN/postrm
    chmod a+x $DEBIAN/postinst $DEBIAN/postrm
}

function build_binary() {
    test -z "${PACKAGE_NAME}" && exit 1
    DEBIAN="${BUILD_DIR}/DEBIAN"
    mkdir $DEBIAN
    cat > "$DEBIAN/control" << EOF
Package: $PACKAGE_NAME
Version: $PACKAGE_VERSION-1
Section: base
Priority: optional
Architecture: all
Depends: nicotine, tor, socat
Maintainer: Lene Preuss <lene.preuss@gmail.com>
Description: Cigorette
 Contains nicotine and tor.
 Connects to the Soulseek filesharing network anonymously.
EOF
    echo "${CIGORETTE_EXE} usr/bin" > $DEBIAN/install
    postinst $DEBIAN
    dpkg-deb --build ${BUILD_DIR} || exit 1
    rm -rf "${BUILD_DIR}"
}

function build_source() {
    test -z "${PACKAGE_VERSION}" && exit 1
    test -z "${MAINTAINER_EMAIL}" && exit 1
    test -z "${LICENSE}" && exit 1
    test -z "${CIGORETTE_URL}" && exit 1
    test -z "${CIGORETTE_DESC}" && exit 1
    test -z "${CIGORETTE_LONGDESC}" && exit 1
    test -z "${PGP_KEY_ID}" && exit 1
    test -z "${DISTRIBUTION}" && exit 1
    cd "${BUILD_DIR}"
    dh_make --indep --email "${MAINTAINER_EMAIL}" --copyright "${LICENSE}" --createorig --yes
    sed -i "s#Section: unknown#Section: web#" debian/control
    sed -i 's#Depends: ${misc:Depends}#Depends: nicotine, tor, socat#' debian/control
    sed -i "s#Homepage: <insert the upstream URL, if relevant>#Homepage: ${CIGORETTE_URL}#" debian/control
    sed -i "s#Description: <insert up to 60 chars description>#Description: ${CIGORETTE_DESC}#" debian/control
    sed -i "s# <insert long description, indented with spaces># ${CIGORETTE_LONGDESC}#" debian/control
    sed -i "s#unstable#${DISTRIBUTION}#" debian/changelog
    sed -i "s#-1)#-1~${DISTRIBUTION})#" debian/changelog
    cat debian/changelog
    sed -i 's#quilt#native#' debian/source/format
    rm debian/*.ex debian/*.EX debian/README.source
    echo "${CIGORETTE_EXE} usr/bin" > debian/install
    postinst debian
    lintian -Evi --profile ubuntu
    debuild -S -us -uc
    cd ..
    debsign -k ${PGP_KEY_ID} "${PACKAGE_NAME}_${PACKAGE_VERSION}-1~${DISTRIBUTION}_source.changes"
#    debsign -k ${PGP_KEY_ID} "${PACKAGE_NAME}_${PACKAGE_VERSION}-1.dsc"
}

if [[ "x${SOURCE}" == "xtrue" ]]; then
    build_source
else
    build_binary
fi

